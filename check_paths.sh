#!/bin/bash
silent=""
if [[ $2 == "-s" || $2 == "--silent" ]]
then
	silent="-s"
fi

for single in `echo "$1" | sed -e 's/:/\n/g'`
do
	echo "Checking $single"
	./check.sh $silent ${single%/}
done
