#!/bin/bash
SYSTEMD_PATHS="/etc/systemd/system:/run/systemd/system:/lib/systemd/system:$XDG_CONFIG_HOME/systemd/user:$HOME/.config/systemd/user:/etc/systemd/user:$XDG_RUNTIME_DIR/systemd/user:/run/systemd/user:$XDG_DATA_HOME/systemd/user:$HOME/.local/share/systemd/user:/usr/lib/systemd/user"
./check_paths.sh $SYSTEMD_PATHS $@
