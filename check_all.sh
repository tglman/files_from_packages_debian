#!/bin/bash

echo "Checking executable in \$PATH"
./check_path.sh $@
echo "Checking systemd services"
./check_systemd_paths.sh $@
echo "Checking cron paths"
./check_cron_paths.sh $@

