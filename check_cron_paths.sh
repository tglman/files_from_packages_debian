#!/bin/bash
CRON_PATHS=""
for path in `ls /etc/cron.*/ -d`
do 
	CRON_PATHS="$CRON_PATHS:$path"
done

./check_paths.sh $CRON_PATHS $@
