#!/bin/bash
# set -x
silent="0"
if [[ $1 == "-s" || $1 == "--silent" ]]
then
	to_check=$2
	silent="1"
else
	to_check=$1
fi

for file in `ls $to_check`
do
	full_file="$to_check/$file"
	solved_file="$to_check/$file"
	while [[ -L $solved_file ]] 
	do
		solved_file=`readlink -f $solved_file`
	done
	search=`dpkg --search $solved_file 2> /dev/null | wc -l ` 
	if [ $search -eq 0 ] 
	then
		if [ "$full_file" == "$solved_file" ]
		then
			echo "File '$full_file' not from a package"
		else 
			echo "File '$full_file' link to '$solved_file' not from a package"
		fi	
	elif [ $silent -eq "0" ]
	then
		echo "OK '$full_file' from a package "
	fi
done

