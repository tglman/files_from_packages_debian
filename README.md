# files_from_packages_debian

Simple set of scripts to check if files in certain paths come from an apt package


## Usage

Default beheaviou check all the cases printing out every file it checked
```
./check_all.sh
```
or print out only the file that do not come from a package
```
./check_all.sh --silent
```

The file checked are all on the paths specified by:  
environment variable "PATH" specific file `check_path.sh`  
systemd units paths specific file `check_systemd_paths.sh` 
cron file paths specific file `check_cron_paths.sh`

